# Revive String Computation

## To run the setup on terminal
```
composer install
```

## To autoload files run on terminal
```
composer dump-autoload
```

### After complete install of dependencies run
```
./vendor/bin/phpunit tests/MyString
```

### Run function reviveString in revivestring.php for quick check functional test