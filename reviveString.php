<?php

function reviveString(string $string, int $k): string {
    function mySubString($string, $charset = 'UTF-8') {
    $length = mb_strlen($string, $charset);
    for ($i = 0; $i < $length; $i++)
      for ($j = 1; $j <= $length; $j++)
        $subs[] = mb_substr($string, $i, $j, $charset);

      $arr = array_unique($subs);

      natsort($arr);

      $newString = implode("", $arr);

      return $newString;

    }

    $sub = mySubString($string);

    $position = $sub{$k-1};

    return $position;
}

 if(valid()) {
     echo('All tests successful!');
 }
 
 
 //Refer the tests on the this root folder

